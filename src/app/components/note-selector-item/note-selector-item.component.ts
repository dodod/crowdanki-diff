import {Component, Input, OnInit} from '@angular/core';
import {NoteDiff} from '../../models/note-diff';

@Component({
  selector: 'app-note-selector-item[noteDiff][selected]',
  templateUrl: './note-selector-item.component.html',
  styleUrls: ['./note-selector-item.component.scss']
})
export class NoteSelectorItemComponent implements OnInit {
  @Input() noteDiff: NoteDiff;
  @Input() selected: boolean;

  constructor() { }

  ngOnInit() {
  }
}
