import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GitlabService} from '../../services/gitlab.service';
import {MatDialog} from '@angular/material/dialog';
import {AuthenticationModalComponent} from '../authentication-modal/authentication-modal.component';
import {Deck} from '../../models/deck';
import {Note} from '../../models/note';
import {MathJaxDirective} from 'ngx-mathjax';
import {NoteDiff} from '../../models/note-diff';

@Component({
  selector: 'app-diff',
  templateUrl: './diff.component.html',
  styleUrls: ['./diff.component.scss']
})
export class DiffComponent implements OnInit {
  @ViewChild('math', { static: false, read: MathJaxDirective })
  mathJax?: MathJaxDirective;

  projectId: number;
  projectHttpUrl: string;
  mergeRequestId: number;
  projectName: string;
  mergeRequestName: string;
  mergeRequestHttpUrl: string;
  incomingDeck: Deck;
  incomingCommitId: string;
  currentDeck: Deck;
  currentCommitId: string;
  deckNames: string[];
  selectedDeck = '';
  selectedNoteIncoming: Note;
  selectedNoteCurrent: Note;
  hideCardsWithoutChanges = true;
  addScrollingClassToCardsDiff = false;


  constructor(
    private route: ActivatedRoute,
    private gitlabService: GitlabService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.projectId = params.project;
      this.mergeRequestId = params.mr;
      this.reloadDiff();
    });
    if (!GitlabService.authenticated) {
      if (!this.gitlabService.authenticateWithCookie()) {
        const dialogRef = this.dialog.open(AuthenticationModalComponent, {
          width: '50%',
        });

        dialogRef.afterClosed().subscribe(result => {
          this.gitlabService.authenticate(result);
          this.reloadDiff();
        });
      } else {
        this.reloadDiff();
      }
    }
  }

  reloadDiff() {
    if (GitlabService.authenticated) {
      this.gitlabService.getProjectName(this.projectId).then(result => {
        this.projectName = result;
      });
      this.gitlabService.getProjectUrl(this.projectId).then(result => {
        this.projectHttpUrl = result;
      });
      this.gitlabService.getMergeRequestName(this.projectId, this.mergeRequestId).then(result => {
        this.mergeRequestName = result;
      });
      this.gitlabService.getMergeRequestUrl(this.projectId, this.mergeRequestId).then(result => {
        this.mergeRequestHttpUrl = result;
      });
      this.gitlabService.getIncomingDeckNames(this.projectId, this.mergeRequestId).then(result => {
        this.deckNames = result;
      });
      if (this.selectedDeck.length > 0) {
        this.gitlabService.getIncomingDeck(this.projectId, this.mergeRequestId, this.selectedDeck).then(result => {
          this.incomingDeck = JSON.parse(result.toString());
        });
        this.gitlabService.getCurrentDeck(this.projectId, this.mergeRequestId, this.selectedDeck).then(result => {
          this.currentDeck = JSON.parse(result.toString());
        });
        this.gitlabService.getCurrentCommitId(this.projectId, this.mergeRequestId).then(result => {
          this.currentCommitId = result;
        });
        this.gitlabService.getIncomingCommitId(this.projectId, this.mergeRequestId).then(result => {
          this.incomingCommitId = result;
        });
      }
    }
  }

  selectDiff(noteDiff: NoteDiff) {
    if (noteDiff.firstExists) {
      this.selectedNoteCurrent = this.currentDeck.notes.find((note: Note) => {
        return note.guid === noteDiff.noteGuid;
      });
    } else {
      this.selectedNoteCurrent = null;
    }
    if (noteDiff.secondExists) {
      this.selectedNoteIncoming = this.incomingDeck.notes.find((note: Note) => {
        return note.guid === noteDiff.noteGuid;
      });
    } else {
      this.selectedNoteIncoming = null;
    }
  }

  cardsDiffScroll() {
    const element = document.getElementById('cards-diff');
    const rect = element.getBoundingClientRect();
    // TODO : Add fix for long cards
    this.addScrollingClassToCardsDiff = rect.top <= 32;
  }
}
