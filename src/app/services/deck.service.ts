import {Injectable} from '@angular/core';
import {DeckDiff} from '../models/deck-diff';
import {Deck} from '../models/deck';
import {Note} from '../models/note';
import {NoteService} from './note.service';

@Injectable({
  providedIn: 'root'
})
export class DeckService {

  constructor(private noteService: NoteService) {
  }


  public diffDecks(firstDeck: Deck, secondDeck: Deck): DeckDiff {
    // TODO : Handle case if one deck is null
    firstDeck.notes.sort((a: Note, b: Note) => (a.guid > b.guid) ? 1 : -1);
    secondDeck.notes.sort((a: Note, b: Note) => (a.guid > b.guid) ? 1 : -1);

    const deckDiff: DeckDiff = {
      noteDiffs: []
    };
    let leftCounter = 0;
    let rightCounter = 0;
    while (leftCounter < firstDeck.notes.length - 1 || rightCounter < secondDeck.notes.length - 1) {
      let leftNoteToDiff: Note;
      let rightNoteToDiff: Note;
      if (firstDeck.notes[leftCounter].guid === secondDeck.notes[rightCounter].guid) {
        leftNoteToDiff = firstDeck.notes[leftCounter];
        rightNoteToDiff = secondDeck.notes[rightCounter];
        if (leftCounter + 1 <= firstDeck.notes.length - 1) {
          leftCounter++;
        }
        if (rightCounter + 1 <= secondDeck.notes.length - 1) {
          rightCounter++;
        }
      } else {
        // find out which note was added
        if ((firstDeck.notes[leftCounter].guid < secondDeck.notes[rightCounter].guid &&
            !(leftCounter === firstDeck.notes.length - 1)) ||
            rightCounter === secondDeck.notes.length - 1
        ) {
          leftNoteToDiff = firstDeck.notes[leftCounter];
          rightNoteToDiff = null;
          if (leftCounter + 1 <= firstDeck.notes.length - 1) {
            leftCounter++;
          }
        } else {
          leftNoteToDiff = null;
          rightNoteToDiff = secondDeck.notes[rightCounter];
          if (rightCounter + 1 <= secondDeck.notes.length - 1) {
            rightCounter++;
          }
        }
      }
      deckDiff.noteDiffs.push(
        this.noteService.diffNotes(
          leftNoteToDiff,
          rightNoteToDiff
        )
      );
    }
    return deckDiff;
  }
}
