import {Pipe, PipeTransform} from '@angular/core';
import {GitlabService} from '../services/gitlab.service';

@Pipe({
  name: 'preprocessField'
})
export class PreprocessFieldPipe implements PipeTransform {
  transform(value: string, ref: string, path: string): string {
    return this.replaceLatex(value, ref, path);
  }

  replaceLatex(input: string, ref: string, path: string): string {
    // TODO : This needs some heeeeeeaaaavyyyy refactoring.....
    const regex = /\[(latex|\$)\](.|\n)*?\[\/(latex|\$)\]/gm;
    const tags = input.match(regex);
    if (tags !== null) {
      tags.forEach(tag => {
        if (tag.includes('[latex]')) {
          let inner = tag.replace('[latex]', '');
          inner = inner.replace('[/latex]', '');

          // Display Math Mode
          let equations = inner.match(/\$\$(.*?)\$\$/gm);
          if (equations !== null) {
            equations.forEach(equation => {
              inner = inner.replace(equation, '\\[ ' + equation.replace(/\$\$/g, '') + ' \\]');
            });
          }

          // Normal math mode
          equations = inner.match(/\$(.*?)\$/gm);
          if (equations !== null) {
            equations.forEach(equation => {
              inner = inner.replace(equation, '\\( ' + equation.replace(/\$/g, '') + ' \\)');
            });
          }
          input = input.replace(tag, inner);
        } else {
          let inner = tag.replace('[$]', '');
          inner = inner.replace('[/$]', '');
          input = input.replace(tag, '\\( ' + inner + ' \\)');
        }
      });
    }
    const sources = input.match(/<img.*?src=['"](.*?)['"]/g);
    if (sources !== null) {
      sources.forEach(source => {
        const imageName = source.match(/<img.*?src=['"](.*?)['"]/);
        input = input.replace(
          source,
          '<img src="https://gitlab.com/' + GitlabService.cachedProject.path_with_namespace + '/raw/' + ref + '/' + path + '/media/' + imageName[1] + '?inline=false"'
        );
      });
    }
    return input;
  }
}
