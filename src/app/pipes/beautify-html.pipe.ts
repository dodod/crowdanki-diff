import { Pipe, PipeTransform } from '@angular/core';
import * as pretty from 'pretty';

@Pipe({
  name: 'beautifyHtml'
})
export class BeautifyHtmlPipe implements PipeTransform {

  transform(value: string): string {
    return pretty(value, {ocd: false});
  }
}
