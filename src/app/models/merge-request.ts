export interface MergeRequest {
  id: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  target_branch: string;
  source_branch: string;
  source_project_id: number;
  target_project_id: number;
  web_url: string;
  diverged_commits_count: number;

  // TODO : Add missing stuff
}
